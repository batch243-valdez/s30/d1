db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

// [Section] MongoDB aggregation
/*
  MongoDB aggregation
    - Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
    - Compared to doing CRUD operations, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application
*/

  // Using the AGGREGATE METHOD

/*
      $match
        - Is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process
        
        Syntax:

            ($match: {filed:value})
*/

        db.fruits.aggregate([
            {$match: {onSale:true}}
          ]);

/*
      $group is used to group elements together and field-value pairs, using the data from the grouped elements

      Syntax:

        {$group: {_id:"value", fieldResult:"valueResult"}}
*/

        db.fruits.aggregate([
            {$group: {_id:"$supplier_id", 
            total: {$sum: "$stock"}}}
            ]);

  // Aggregating Documents using 2 pipeline stages

  /*
      Syntax:

        db.collectionName.aggregate([
          {$match: {field:value}},
          {$group: {_id:"value", fieldResult:"valueResult"}}
        ])
  */

    db.fruits.aggregate([
        {$match: {onSale:true}},
        {$group: {_id:"$supplier_id", 
        total: {$sum: "$stock"}}}
      ]);